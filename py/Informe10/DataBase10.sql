--crear base de datos
CREATE DATABASE informe10
GO
--seleccionar base de datos
USE informe10
GO
--crear participantes
CREATE SCHEMA servidor
GO
CREATE SCHEMA cliente
GO
--crear tablas y parametros
CREATE TABLE cliente.Sensor (
    id_Sensor INT IDENTITY (1,1) PRIMARY KEY,
    Temperatura INT NOT NULL,
    Humedad INT NOT NULL,
    SampleTime DATETIME
)
GO
CREATE TABLE servidor.Acceso (
    FechaDeAcceso DATETIME
)
GO