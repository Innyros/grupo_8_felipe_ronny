import socket
import time
from uuid import getnode

HEADER = 64
PORT = 2222
SERVER = 'localhost'
ADDR = (SERVER, PORT)
FORMAT = 'utf-8'
DISCONNECT_MESSAGE = 'DISCONNECT!'

client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

client.connect(ADDR)


def send(msg):
	largo=len(msg)
	print('Enviando: ',msg)

	largo_2=str(largo).encode(FORMAT)

	largo_2 +=b' '*(HEADER-len(largo_2))
	#code ini
	c=''
	for i in range(len(msg)):
		x=ord(msg[i])-1 #caracter->ASCII
		x=str(x)
		if len(x)==2:
			c+='0'
		c+=x
	msg=c
	msg=msg.encode(FORMAT)
	#code fin
	client.send(msg)
	time.sleep(0.5)	#espera para dar tiempo al servidor
	client.send(largo_2)
	print(client.recv(2048).decode(FORMAT))

#programa
#MAC ini
mac=hex(getnode())
send(mac)
#MAC fin
send('habia')
send('una')
send('vez')
send('algo')
send(DISCONNECT_MESSAGE)
