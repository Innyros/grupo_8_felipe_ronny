import socket
import threading

HEADER = 64
PORT = 2222
SERVER = 'localhost'
ADDR = (SERVER, PORT)
FORMAT = 'utf-8'
DISCONNECT_MESSAGE = 'DISCONNECT!'
PASS=0
MAC='0x201a068d4155'

server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

server.bind(ADDR)

def handle_client(conn, addr):
	print(f"[NUEVA CONEXION] {addr} ESTA CONECTADO.")

	connected = True
	while connected:
		msg_length = conn.recv(HEADER).decode(FORMAT)
		if msg_length:
			msg = conn.recv(HEADER)
			#decode ini
			y=''
			z=''
			i=0
			while i<int(len(msg_length)):
				for j in range(3):
					y+=msg_length[i+j]
				z+=chr(int(y)+1) #ASCII->caracter
				y=''
				i+=3
			msg_length=z
			#decode fin
			#MAC ini
			global PASS	#para usar var global
			if msg_length == MAC: #detecta MAC valida
				PASS=1
			if PASS==0:	#detecta MAC invalida
				msg_length=DISCONNECT_MESSAGE
				print('Usuario no permitido')
			#MAC fin
			if msg_length == DISCONNECT_MESSAGE:
				connected = False
				#print('false')
				PASS=0
			print('Nuevo mensaje: ',msg_length)
			conn.send("Msg received".encode(FORMAT))
	conn.close()

def start():
	server.listen()
	print(f"[ESCUCHANDO] Server is listening on address {ADDR}")
	while True:
		conn, addr = server.accept()
		thread = threading.Thread(target=handle_client, args=(conn, addr))
		thread.start()
		print(f"[CONEXION ACTIVA] {threading.activeCount() - 1}")

#programa
print("[COMENZANDO] server is running.....")
start()


